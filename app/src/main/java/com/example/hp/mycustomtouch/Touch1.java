package com.example.hp.mycustomtouch;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Touch1 extends Activity implements View.OnTouchListener {

    GradientDrawable drawable;
    View viewCircle;
    RelativeLayout layout;
    float radius;
    float xCenter;
    float yCenter;
    TextView display;
    Button nextactivity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch1);


        layout = (RelativeLayout) findViewById(R.id.relative_circle);
        viewCircle = findViewById(R.id.view_circle);
        drawable = (GradientDrawable) viewCircle.getBackground();
        display = (TextView) findViewById(R.id.on_touch_status);
            nextactivity = (Button) findViewById(R.id.bt_next_level);

        layout.setOnTouchListener(Touch1.this);

nextactivity.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent it = new Intent(Touch1.this, Touch2.class);
        startActivity(it);
        finish();
    }
});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_touch1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        centreis();

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                process(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_DOWN:
                process(event.getX(), event.getY());
                break;
            case MotionEvent.ACTION_MOVE:
                process(event.getX(), event.getY());

                break;
        }
        return true;
    }


    public void process(float currX, float currY) {
        double dist;
        dist = distanceis(currX, currY);
        if (dist < radius) {
            toggleColor(1);
        } else if (dist == radius) {
            toggleColor(2);
        } else {
            toggleColor(3);
        }
    }

    public void toggleColor(int pos) {


        switch (pos) {
            case 1:
                drawable.setColor(Color.RED);
                display.setText("Inside The Circle");
                break;
            case 2:
                drawable.setColor(Color.BLACK);
                display.setText("On Boundary of The Circle");
                break;
            case 3:
                drawable.setColor(Color.GREEN);
                display.setText("Outside The Circle");
                break;
        }
    }

    public double distanceis(float xPos, float yPos) {
        double distance;
        distance = Math.sqrt(Math.pow((xPos - xCenter), 2) + Math.pow((yPos - yCenter), 2));
        return distance;
    }

    public void centreis() {
        radius = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
        xCenter = (viewCircle.getRight() + viewCircle.getLeft()) / 2;
        yCenter = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
    }
}
