package com.example.hp.mycustomtouch;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

public class Touch2 extends Activity implements View.OnClickListener, View.OnTouchListener {


    GridView gridView;
    GradientDrawable gradientDrawable;
    MyGridAdapter adapter;
    View viewCircle;
    float radius;
    float xCenter;
    float yCenter;
    Boolean flag = false;
    TextView display;
    int numRows = 2, numCols = 3;
    Button nextacti, createcircle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch2);


        createcircle = (Button) findViewById(R.id.circle);
        createcircle.setOnClickListener(this);
        nextacti = (Button) findViewById(R.id.bt_next_level);
        gridView = (GridView) findViewById(R.id.grid_show);
        display = (TextView) findViewById(R.id.on_touch_status);
         float gridViewHeight = gridView.getHeight();
         float gridViewWidth = gridView.getWidth();

        adapter = new MyGridAdapter(Touch2.this, numRows, numCols, gridViewHeight, gridViewWidth);
        nextacti.setOnClickListener(Touch2.this);

           gridView.setNumColumns(numCols);
        gridView.setScrollContainer(false);
        gridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        gridView.setOnTouchListener(Touch2.this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_next_level:
                Intent it = new Intent(Touch2.this, Touch3.class);
                startActivity(it);
                finish();
                break;


            case R.id.circle:
                float gridViewHeight = gridView.getHeight(), gridViewWidth = gridView.getWidth();
                gridView.setNumColumns(numCols);
                adapter = new MyGridAdapter(Touch2.this, numRows, numCols, gridViewHeight, gridViewWidth);

                gridView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                gridView.setOnTouchListener(Touch2.this);
        }
    }

    public void setCircleInfo(int pos) {
        viewCircle = gridView.getChildAt(pos);
        gradientDrawable = (GradientDrawable) viewCircle.getBackground();
        radius = (gradientDrawable.getIntrinsicWidth()) / 2;
        setCenter();
    }

    public void setCenter() {
        xCenter = (viewCircle.getRight() + viewCircle.getLeft()) / 2;
        yCenter = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
    }

    public double getDistance(float xPos, float yPos) {
        double distance;
        distance = Math.sqrt(Math.pow((xPos - xCenter), 2) + Math.pow((yPos - yCenter), 2));
        return distance;
    }

    public void toggleColor(int pos) {

        switch (pos) {
            case 1:
                gradientDrawable.setColor(Color.GREEN);

                display.setText("Inside The Circle");
                break;
            case 3:

                gradientDrawable.setColor(Color.YELLOW);
                              display.setText("Outside The Circle");
                            break;
        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        int action = event.getActionMasked();
        float currentXPosition = event.getX();
        float currentYPosition = event.getY();
        int position = gridView.pointToPosition((int) currentXPosition, (int) currentYPosition);
        // Toast.makeText(this, "rchd here", Toast.LENGTH_SHORT).show();
        if (position < 0) {
            display.setText("Outside The Circle");
            return false;
        }

        setCircleInfo(position);
        double dist = getDistance(event.getX(), event.getY());
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_UP:
                if (dist < radius) {
                    toggleColor(1);
                } else if (dist == radius) {
                    toggleColor(2);
                } else {
                    toggleColor(3);
                }
                break;
            case MotionEvent.ACTION_DOWN:
                if (dist < radius) {
                    toggleColor(1);
                } else if (dist == radius) {
                    toggleColor(2);
                } else {
                    toggleColor(3);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (dist <= radius) {
                    toggleColor(1);
                } else {
                    toggleColor(3);
                }
                break;
        }
        return true;
    }
}

