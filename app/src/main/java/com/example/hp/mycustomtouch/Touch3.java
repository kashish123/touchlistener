package com.example.hp.mycustomtouch;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class Touch3 extends Activity implements View.OnClickListener ,View.OnTouchListener {




    TextView answer;
    Button nextactivity;
    int numRows, numCols;
    EditText enterrows, entercols;
    MyGridAdapter gridViewAdapter;
    View viewCircle;
    float radius;
    float xCenter;
    float yCenter;
    GridView gridView;
    GradientDrawable gradientDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch3);


        Toast.makeText(this, "Rows and columns should be max 3", Toast.LENGTH_SHORT).show();


        gridView = (GridView) findViewById(R.id.grid_show);
        answer = (TextView) findViewById(R.id.on_touch_status);

        enterrows = (EditText) findViewById(R.id.et_num_rows);
        entercols = (EditText) findViewById(R.id.et_num_cols);
        nextactivity = (Button) findViewById(R.id.submit);


        nextactivity.setOnClickListener(Touch3.this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_touch3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                numRows = Integer.parseInt(enterrows.getText().toString());
                numCols = Integer.parseInt(entercols.getText().toString());
                if ((numRows > 0 && numRows < 4) || (numCols > 0 && numCols < 4)) {
                    float gridViewHeight = gridView.getHeight(), gridViewWidth = gridView.getWidth();
                    gridView.setNumColumns(numCols);
                    gridViewAdapter = new MyGridAdapter(Touch3.this, numRows, numCols, gridViewHeight, gridViewWidth);

                    gridView.setAdapter(gridViewAdapter);
                    gridViewAdapter.notifyDataSetChanged();
                    gridView.setOnTouchListener(Touch3.this);
                } else
                    answer.setText("Enter Correct Values");
                break;


    }}


    public void setCircleInfo(int pos) {
        viewCircle = gridView.getChildAt(pos);
        gradientDrawable = (GradientDrawable) viewCircle.getBackground();
        radius = (gradientDrawable.getIntrinsicWidth()) / 2;
        setCenter();
    }

    public void setCenter() {
        xCenter = (viewCircle.getRight() + viewCircle.getLeft()) / 2;
        yCenter = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
    }

    public double getDistance(float xPos, float yPos) {
        double distance;
        distance = Math.sqrt(Math.pow((xPos - xCenter), 2) + Math.pow((yPos - yCenter), 2));
        return distance;
    }

    public void toggleColor(int pos) {
        switch (pos) {
            case 1:
                gradientDrawable.setColor(Color.GREEN);
                answer.setText("Inside The Circle");
                break;
            case 3:
                gradientDrawable.setColor(Color.YELLOW);
                gradientDrawable.setColor(Color.YELLOW);
                answer.setText("Outside The Circle");
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getActionMasked();
        float currentXPosition = event.getX();
        float currentYPosition = event.getY();
        int position = gridView.pointToPosition((int) currentXPosition, (int) currentYPosition);
        if (position < 0) {
            answer.setText("Outside The Circle");
            return false;
        }
        setCircleInfo(position);
        double dist = getDistance(event.getX(), event.getY());
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_UP:
                if (dist < radius) {
                    toggleColor(1);
                } else if (dist == radius) {
                    toggleColor(2);
                } else {
                    toggleColor(3);
                }
                break;
            case MotionEvent.ACTION_DOWN:
                if (dist < radius) {
                    toggleColor(1);
                } else if (dist == radius) {
                    toggleColor(2);
                } else {
                    toggleColor(3);
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (dist <= radius) {
                    toggleColor(1);
                } else {
                    toggleColor(3);
                }
                break;
        }

        return true;
    }

    }

