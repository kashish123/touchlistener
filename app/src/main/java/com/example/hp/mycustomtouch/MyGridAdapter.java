package com.example.hp.mycustomtouch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

/**
 * Created by HP on 10/12/2015.
 */
public class MyGridAdapter extends BaseAdapter {

    Context context;
    int numRows, numCols,width, height;

    public MyGridAdapter(Context context, int numRows, int numCols,float height,float width) {
        this.context = context;
        this.numRows = numRows;
        this.numCols = numCols;
        this.width= (int) (width/numCols);
        this.height= (int) (height/numRows);
    }




    @Override
    public int getCount() {
        return numCols*numRows;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, height);
        LayoutInflater li = LayoutInflater.from(context);
        convertView = li.inflate(R.layout.grid_column_layout, parent, false);
        convertView.setLayoutParams(params);
        return convertView;
    }
}
